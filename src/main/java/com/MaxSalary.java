package com;

import java.util.stream.Stream;

/**
 * Interface to calculate max salary {@link MaxSalary}
 */
@FunctionalInterface
public interface MaxSalary {

    Integer findMaxSalary(Stream<Employee> employeeStream);
}
