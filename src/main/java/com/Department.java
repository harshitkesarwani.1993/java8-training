package com;

import java.util.ArrayList;
import java.util.List;

/**
 * Class to store department information {@link Department}
 */
public class Department {

    //  Variables and Constants
    private String deptId;

    private String deptName;

    private List<Employee> employees = new ArrayList<>();

    //  Getters and Setters
    public String getDeptId() {
        return deptId;
    }

    public Department setDeptId(String deptId) {
        this.deptId = deptId;
        return this;
    }

    public String getDeptName() {
        return deptName;
    }

    public Department setDeptName(String deptName) {
        this.deptName = deptName;
        return this;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public Department setEmployees(List<Employee> employees) {
        this.employees = employees;
        return this;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Department{");
        sb.append("deptId='").append(deptId).append('\'');
        sb.append(", deptName='").append(deptName).append('\'');
        sb.append(", employees=").append(employees);
        sb.append('}');
        return sb.toString();
    }
}
