package com;

import java.io.File;
import java.io.FileFilter;

public class MethodReferenceExample {

    public File[] getHiddenFiles(){

        File[] hiddenFiles = new File(".").listFiles(new FileFilter() {
                                                         @Override
                                                         public boolean accept(File file) {
                                                             return file.isHidden();
                                                         }
                                                     });

//        File[] hiddenFiles = new File(".").listFiles(File::isHidden);
        return hiddenFiles;
    }
}
