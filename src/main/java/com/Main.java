package com;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

/**
 * Main Class {@link Main}
 * */
public class Main {

    public static void main(String[] args) {
        List<Department> departments = new ArrayList<>();
        departments.add(insertDepartment(10));
        departments.add(insertDepartment(20));
        System.out.println(departments);
        MaxSalary maxSalary =
                (Stream<Employee> e) -> e.max(Comparator.comparing(Employee::getSalary)).orElseGet(Main::getDefaultMAxSalary).getSalary();
        departments.forEach(d -> System.out.println(maxSalary.findMaxSalary(d.getEmployees().stream())));
    }

    private static Department insertDepartment(int i) {
        return new Department().setDeptId(String.valueOf(i))
                               .setDeptName("Department" + i)
                               .setEmployees(insertEmployee(i * 2));
    }

    private static List<Employee> insertEmployee(int i) {
        List<Employee> employees = new ArrayList<>();
        for (int j = 0; j < 3; j++) {
            Employee employee = new Employee();
            employee.setId(String.valueOf(i))
                    .setName("Employee" + (i + 1))
                    .setSalary((int) (Math.random() * 99 + 1));
            employees.add(employee);
            i++;
        }
        return employees;
    }

    private static Employee getDefaultMAxSalary(){
        return new Employee().setSalary(0);
    }

}
